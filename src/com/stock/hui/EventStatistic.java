package com.stock.hui;

import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicInteger;

public class EventStatistic implements Statistic{
    //use ConcurrentLinkedDeque to keep adding event value, and it uses Compare And Switch to make sure thread safe
    ConcurrentLinkedDeque<Integer> queue = new ConcurrentLinkedDeque<>();
    //use AtomicInteger achieve value safe
    AtomicInteger max_value = new AtomicInteger(Integer.MIN_VALUE);
    AtomicInteger min_value = new AtomicInteger(Integer.MAX_VALUE);
    AtomicInteger sum_value = new AtomicInteger();
    AtomicInteger square_sum = new AtomicInteger();

    @Override
    public synchronized void event(int value) {
        //add value to event
        queue.add(value);
        //set value
        if (max_value.intValue() < value){
            max_value.set(value);
        } else if (value < min_value.intValue()){
            min_value.set(value);
        }
        sum_value.set(sum_value.intValue() + value);
        square_sum.set(sum_value.intValue() + value * value);

    }

    @Override
    //include division consider is thread threat
    public synchronized float mean() {
        return sum_value.floatValue()/queue.size();
    }

    @Override
    public int minimum() {
        return min_value.intValue();
    }

    @Override
    public int maximum() {
        return max_value.intValue();
    }

    @Override
    public synchronized float variance() {
        float mean_value =  mean();
        return (square_sum.floatValue() - queue.size() + mean_value *mean_value) / (queue.size() - 1);
    }
}
