package com.stock.hui;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

public class MyAccount {
    private final String name;
    private final Map<Transaction, Integer> list;

    public MyAccount(String name) {
        this.name = name;
        this.list = new TreeMap<>();
    }

    public int addToMyAccount(Transaction item, int addedQuantity){
        if ((item != null) && (addedQuantity > 0)){
            int inMyAccount = list.getOrDefault(item, 0);
            list.put(item, inMyAccount + addedQuantity);
            return inMyAccount;
        }
        return 0;
    }

    public Map<Transaction, Integer> Items(){
        return Collections.unmodifiableMap(list);
    }

    @Override
    public String toString() {
        String s = "\nMy Account " + name + " contains " + list.size() + ((list.size() == 1 ? " item" : " items") + "\n");

        double totalCost = 0.0;
        for (Map.Entry<Transaction, Integer> item : list.entrySet()){
            s = s + item.getKey() + ". " + item.getValue() + " sold\n";
            totalCost += item.getKey().getPrice() * item.getValue();
        }
        return s + "Total cost " + totalCost;
    }
}
