package com.stock.hui;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.net.PortUnreachableException;
import java.util.Map;

public class Main {
    public static TransactionStatistics transactionList = new
            TransactionStatistics();

    public static void main(String[] args) {
	// write your code here

     //   TransactionStatistics transactionStatistic1 = new TransactionStatistics();

      //  RunTimeData rd1 = new RunTimeData();


        Person tempPerson = new Person("Lucky",30);
        tempPerson = new Person("Jason",50);
        tempPerson = new Person("Judy",40);
        System.out.println(tempPerson);

        Employee tempEmployee = new Employee("Vicky",53,"Leader");
        tempEmployee = new Employee("Yasser",34,"Manager");
        tempEmployee = new Employee("Lucas",40,"Manager");
        System.out.println(tempEmployee);

        //test add items to Transaction List
        Transaction tempTransaction =  new Transaction("AAPL",100,175);
        transactionList.addStock(tempTransaction);

        tempTransaction =  new Transaction("AI",600,30);
        transactionList.addStock(tempTransaction);

        tempTransaction =  new Transaction("TSLA",50,951);
        transactionList.addStock(tempTransaction);
        System.out.println(tempTransaction);

        for (String s: transactionList.Items().keySet()){
            System.out.println(s);
        }
        System.out.println("=======================");
        //test sell items
        MyAccount myAccount = new MyAccount("Hui");
        sellItem(myAccount,"AI",100);
        System.out.println(myAccount);
        sellItem(myAccount,"AI",500);
        System.out.println(myAccount);

        if (sellItem(myAccount,"AI",500) != 500){
            System.out.println("There are no more in stock");
        }

        System.out.println("=======================");
        //test adjust quantity
        transactionList.Items().get("AAPL").adjustStock(100);
        transactionList.get("AAPL").adjustStock(-150);
        System.out.println(transactionList);
        for (Map.Entry<String,Double> price: transactionList.PriceList().entrySet()){
            System.out.println(price.getKey() + " costs " + price.getValue());
        }

        System.out.println("=======================");
        EventStatistic eventStatistic = new EventStatistic();
        eventStatistic.event(6);
        System.out.println(eventStatistic.mean());
        System.out.println(eventStatistic.maximum());
        System.out.println(eventStatistic.minimum());
        System.out.println(eventStatistic.variance());

    }

    public static int sellItem(MyAccount myAccount, String item, int quantity){
        //retrieve the item from transaction list
        Transaction stockItem = transactionList.get(item);
        if (stockItem == null) {
            System.out.println("Invalid stock " + item);
            return 0;
        }
        if (transactionList.sellStock(item,quantity) !=0 ){
            myAccount.addToMyAccount(stockItem, quantity);
            return quantity;
        }
        return 0;
    }

}

