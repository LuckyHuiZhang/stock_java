/*
 * Copyright (c) 2006-2019 Henri Tremblay.
 */
package com.stock.hui;

import java.util.Objects;

public class Employee extends Person{
    private final String role;

    public Employee(String name, int age, String role) {
        super(name, age);
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Employee employee = (Employee) o;
        return Objects.equals(role, employee.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), role);
    }

    @Override
    public String toString() {
        return "Employee: " + getName() +
                " is " + getAge() + " years old" +
                " and role is " + role;
    }
}
