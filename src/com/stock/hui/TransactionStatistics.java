/*
 * Copyright (c) 2006-2019 Henri Tremblay.
 */
package com.stock.hui;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Please implement the {@link #mostPopularTicker(List)} method. With a sequential and a parallel algorithm.
 * DO NOT modify the existing code.
 */
public class TransactionStatistics {

   /**
    * Return the most popular ticker in terms of transaction total absolute value (i.e. abs(price * quantity)). For example,
    * let's say we have these transactions:
    * <ul>
    *     <li>transaction("a", -10, 2.0)</li>
    *     <li>transaction("a", 20, 1.0)</li>
    *     <li>transaction("b", 5, 1.0)</li>
    *     <li>transaction("b", 10, 1.0)</li>
    *     <li>transaction("b", 10, 1.0)</li>
    * </ul>
    * The most popular one ticker is "a" with a traded value of 40 compared to b that only has 25.
    *
    //* @param list List containing all transactions we want to look at
    * @return the most popular ticker
    */

   private final Map<String, Transaction> list;


   public TransactionStatistics()  {
      this.list = new LinkedHashMap<>();
   }

   public int addStock(Transaction item){
      if (item != null) {
         // check if already have quantities of this item
         Transaction inStock = list.getOrDefault(item.getTicker(), item);
         // If there are already stocks on this item, adjust the quantity
         if (inStock != item) {
            item.adjustStock(inStock.getQuantity());
         }
         list.put(item.getTicker(),item);
         return item.getQuantity();
      }
      return 0;
   }

   public int sellStock(String item, int addedQuantity){
      Transaction inStock = list.getOrDefault(item, null);

      if ((inStock != null) && (inStock.getQuantity() >= addedQuantity) && (addedQuantity > 0)){
         inStock.adjustStock(-addedQuantity);
         return addedQuantity;
      }
      return 0;
   }

   //create a get() to find key(ticker) from Map<>
   public Transaction get(String key){
      return list.get(key);
   }

   //safely store the priceList to Map<>
   public Map<String, Double> PriceList(){
      Map<String, Double> prices = new LinkedHashMap<>();
      for (Map.Entry<String, Transaction> item : list.entrySet()){
         prices.put(item.getKey(), item.getValue().getPrice());
      }
      return Collections.unmodifiableMap(prices);
   }


   //safely store the transaction to Map<>
   public Map<String, Transaction> Items(){
      return Collections.unmodifiableMap(list);
   }

   @Override
   public String toString() {
      String s = "\nStock List\n";
      double totalCost = 0.0;
      //calculate the total value
      for (Map.Entry<String, Transaction> item : list.entrySet()){
         Transaction transaction = item.getValue();
         double itemValue = transaction.getPrice() * transaction.getQuantity();

         s = s + transaction + ". There are " + transaction.getQuantity() + " in stock. Value of items: ";
         s = s + String.format("%.2f", itemValue) + "\n";
         totalCost += itemValue;
      }
      return s + "Total stock value " + totalCost;
   }


   //single-thread
   public static String mostPopularTicker(List<Transaction> transactions) {
      //map each transaction
      Map<String, Double> map =  new HashMap<>();
      for (Transaction t: transactions) {
         if (map.containsKey(t.getTicker())) {
            map.put(t.getTicker(), map.get(t.getTicker()) + Math.abs(t.getPrice()) * t.getQuantity());
         } else {
            map.put(t.getTicker(), Math.abs(t.getPrice()) * t.getQuantity());
         }
      }
         //find maximum value and most popular ticker
         String mos_ticker = null;
         double max_val = Double.MIN_VALUE;
         for (String t: map.keySet()){
            if (map.get(t) > max_val){
               mos_ticker = t;
               max_val = map.get(t);
            }
         }

       return mos_ticker;
   }

   //multiple-thread
   public static String mostPopularTickerParallel(List<Transaction> transactions) {
      //invoke RunTimeData class
      final RunTimeData rd = new RunTimeData();

      int threadCount = rd.getThreadCount(transactions);
      System.out.println("Count thread number: " + threadCount);
      //calculate length per thread
      final int lengthPerThread = transactions.size() / threadCount;
      //implement tread safe
      Map<String, Double> map = new ConcurrentHashMap<>();
      for (int i = 0; i < threadCount; i++){
         final int index = i;
         new Thread(){
            @Override
            public void run() {
               super.run();
               int start = index * lengthPerThread;
               int end = start + lengthPerThread;
               for (int j=start; j<end; j++){
                  Transaction t = transactions.get(j);
                  if(map.containsKey(t.getTicker())){
                     map.put(t.getTicker(),map.get(t.getTicker())+Math.abs(t.getPrice())*t.getQuantity());
                  }else {
                     map.put(t.getTicker(), Math.abs(t.getPrice()) * t.getPrice());
                  }
               }
               //make sure tread safe of the whole process
               synchronized (rd){
                  rd.completeThreadCount++;
               }
            };
         }.start();
      }
      //calculate the remain tread of transactions
      int remain = transactions.size() % threadCount;
      System.out.println("Count remain transaction: " + remain);
      for (int i = transactions.size() - remain; i < transactions.size(); i ++){
         Transaction t = transactions.get(i);
         if(map.containsKey(t.getTicker())){
            map.put(t.getTicker(),map.get(t.getTicker())+Math.abs(t.getPrice())*t.getQuantity());
         }else {
            map.put(t.getTicker(), Math.abs(t.getPrice()) * t.getPrice());
         }
      }

      //wait for the whole process complete
      while (rd.completeThreadCount != threadCount){
         try {
            Thread.sleep(1);
         }catch (InterruptedException e){
            e.printStackTrace();
            break;
         }
      }
      //find maximum value and most popular ticker
      String mos_ticker = null;
      double max_val = Double.MIN_VALUE;
      for (String t: map.keySet()){
         if (map.get(t) > max_val){
            mos_ticker = t;
            max_val = map.get(t);
         }
      }
      return mos_ticker;
   }
}

class RunTimeData {
   int defaultThreadCount = 10;
   int completeThreadCount = 0;

   public int getThreadCount (List<Transaction> transactions){
      return Math.min(transactions.size(),defaultThreadCount);

   }

}