/*
 * Copyright (c) 2006-2019 Henri Tremblay.
 */
package com.stock.hui;

import java.util.Objects;

/**
 * Class representing a really simplistic transaction. DO NOT modify the existing code. However, you can
 * add fields and methods as you feel it.
 */
public class Transaction implements Comparable<Transaction> {

   public static Transaction transaction(String ticker, int quantity, double price) {
      return new Transaction(ticker, quantity, price);
   }

   /**
    * Symbol identifying the financial instrument
    */
   private final String ticker;

   /**
    * Number of financial instruments exchanged by the transaction. A positive value is a buy, a negative value is a sale
    */
   //changed quantity to not final cause need to adjust quantity in transaction
   private int quantity;

   /**
    * Price paid / sold per financial instrument. Always positive
    */
   private double price;

   public Transaction(String ticker, int quantity, double price) {
      this.ticker = Objects.requireNonNull(ticker);
      this.quantity = quantity;
      this.price = price;
   }

   //define a new constructor and assign a value 0 to quantity for adjust quantity
   public Transaction(String ticker, double price) {
      this.ticker = ticker;
      this.quantity = 0;
      this.price = price;
   }

   public String getTicker() {
      return ticker;
   }

   public int getQuantity() {
      return quantity;
   }

   public double getPrice() {
      return price;
   }

   public void adjustStock(int addedQuantity) {
      int newQuantity = this.quantity + addedQuantity;
      if (newQuantity >= 0) {
          this.quantity= newQuantity;
      }
   }

   @Override
   public boolean equals(Object o) {
      System.out.println("Entering StockItem.equals");
      if (this == o) {
         return true;
      }
      if (o == null || this.getClass() != o.getClass()){
         return false;
      }
      String objName = ((Transaction) o).getTicker();
      return this.ticker.equals(objName);
   }

   @Override
   public int hashCode() {
      return this.ticker.hashCode() + 31;
   }


   @Override
   public int compareTo(Transaction o) {
      System.out.println("Entering StockItem.compareTo");
      if (this == o){
         return 0;
      }
      if (o != null){
         return this.ticker.compareTo(o.getTicker());
      }
      throw  new NullPointerException();
   }

   @Override
   public String toString() {
      return "Transaction{" +
              "ticker='" + this.ticker + '\'' +
              ", quantity=" + this.quantity +
              ", price=" + this.price +
              '}';
   }
}
